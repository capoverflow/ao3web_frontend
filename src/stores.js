import { writable } from 'svelte/store';

export const store = writable({
	WorkID: '21116591',
	ChapterID: '',
	AppJWTToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjE3ODk2MDMzLCJpZGVudGl0eSI6ImVuZGVyIn0.BegTaWSRvaIeHLPaEQw40qzCHondGaGvksUzyFU1Khc'
})
